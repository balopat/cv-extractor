
(import [org.apache.pdfbox.pdmodel.PDDocument]
         [org.apache.pdfbox.util.PDFTextStripper]
         )

 (import java.net.URL)

(defn text-of-pdf
  [url]
  (with-open [pd (PDDocument/load (URL. url))]
    (let [stripper (PDFTextStripper.)]
    (.getText stripper pd))))

(text-of-pdf "file:///Users/balopat/Downloads/HackMIT_Resumes_2015/Zygimantas Straznickas - Zygimantas_Straznickas_2015.pdf")